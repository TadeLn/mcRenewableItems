let verbose = false;



// Get the contents of an external file
function downloadFile(url, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", url, true); // true for asynchronous 
    xmlHttp.send(null);
    return undefined;
}

// Creates and clicks a new "a" element
function goToLink(url) {
    let a = document.createElement("a");
    a.href = url;
    document.body.appendChild(a);
    a.click();
    a.remove();
}

// "data" can be a string (with namespaced item id), an object (with {item: namespacedItemId, count: itemCount}), or an array of those
function itemData(data, array = false) {
    if (Array.isArray(data)) {
        let result = [];
        for (element of data) {
            if (Array.isArray(element)) {
                let subresult = []
                element.forEach((x) => {
                    if (typeof x === "string") {
                        subresult.push({item: x, count: 1});
                    }
                    if (typeof x === "object") {
                        subresult.push(x);
                    }
                });
                result.push(subresult);
            } else {
                if (typeof element === "string") {
                    result.push({item: element, count: 1});
                }
                if (typeof element === "object") {
                    result.push(element);
                }
            }
        }
        return result;
    } else {
        if (array) return itemData([data]);
        if (typeof data === "string") {
            return {item: data, count: 1};
        }
        if (typeof data === "object") {
            return data;
        }
    }
}

// Creates a HTML element with method data
function createMethodListElement(methodId, number = undefined) {
    let link = `#method#${methodId}`;

    let listElement = document.createElement("div");
    listElement.classList.add("listElement");
    listElement.classList.add("mcWidgetSlot");
    listElement.classList.add("clickable");
    if (getMethodRenewability(methodId)) {
        listElement.classList.add("renewable");
    }
    listElement.setAttribute("link", link)
    listElement.addEventListener("click", () => {goToLink(listElement.getAttribute("link"))});

        let icon = document.createElement("div");
        icon.classList.add("icon");
        icon.style.backgroundImage = `url('assets/img/${methods[methodId].icon}.png')`;
        if (number !== undefined) {
            let listNumber = document.createElement("span");
            listNumber.classList.add("listElementNumber");
            listNumber.innerText = number + ".";
            icon.appendChild(listNumber);
        }
        listElement.appendChild(icon);

        let name = document.createElement("a");
        name.classList.add("name");
        name.href = link;
        name.innerText = methods[methodId].name;
        listElement.appendChild(name);
        
        listElement.appendChild(document.createElement("br"));

        let description = document.createElement("span");
        description.classList.add("description")
        description.innerText = methods[methodId].description;
        listElement.appendChild(description);

    return listElement;
}

// Creates a HTML element with item data
function createItemListElement(itemId, number = undefined, count = undefined, cardMode = false) {
    let renewable = getItemRenewability(itemId);
    let nonSurvival, itemName, link;
    if (itemId[0] == "#") {
        nonSurvival = !tags[itemId].survival;
        itemName = tags[itemId].name;
        link = `#tag#${itemId.substr(1)}`;
    } else {
        nonSurvival = !items[itemId].survival;
        itemName = items[itemId].name;
        link = `#item#${itemId}`;
    }

    let listElement = document.createElement("div");
    listElement.classList.add("listElement");
    listElement.classList.add("mcWidgetSlot");
    listElement.classList.add("clickable");
    if (cardMode) listElement.classList.add("itemCard");
    if (renewable) listElement.classList.add("renewable");
    if (nonSurvival) listElement.classList.add("notRenewable");
    listElement.setAttribute("link", link)
    listElement.addEventListener("click", () => {goToLink(listElement.getAttribute("link"))});

        let icon = document.createElement("div");
        icon.classList.add("icon");
        if (itemId[0] == "#") {
            icon.style.backgroundImage = `url('assets/img/tag/${itemId.substr(1).replace(":", "/")}.png')`;
        } else {
            icon.style.backgroundImage = `url('assets/img/item/${itemId.replace(":", "/")}.png')`;
        }
        if (number !== undefined) {
            let listNumber = document.createElement("span");
            listNumber.classList.add("listElementNumber");
            listNumber.innerText = number + ".";
            icon.appendChild(listNumber);
        }
        if (count !== undefined && count != 1) {
            let itemCount = document.createElement("span");
            itemCount.classList.add("itemCount");
            itemCount.innerText = count;
            icon.appendChild(itemCount);
        }
        listElement.appendChild(icon);

        let label = document.createElement("div");
        label.classList.add("label");

            let name = document.createElement("a");
            name.classList.add("name");
            name.href = link
            name.innerText = itemName
            label.appendChild(name);

            label.appendChild(document.createElement("br"));

            let id = document.createElement("span");
            id.classList.add("id");
            id.innerText = itemId;
            label.appendChild(id);

        listElement.appendChild(label);

    return listElement;

}


// All Data
let items, methods, tags;
let numberOfRenewableItems, numberOfNonSurvivalItems;
let __lastCount = {};



// Calculates item renewability
function getItemRenewability(namespacedId, count = 1) {
    // Check if it's a tag and not an item
    if (namespacedId[0] == "#") {
        return getTagRenewability(namespacedId);
    }

    if (verbose) console.groupCollapsed(`Item \t| ${namespacedId} (x${count}):`);

    if (items[namespacedId] === undefined) {
        if (verbose) console.error(`There is no item with id ${namespacedId} (returning false)`);
        if (verbose) console.groupEnd();
        return false;
    }

    // If it's already calculated, use the calculated value
    if (items[namespacedId].renewable !== undefined) {
        if (verbose) console.log(`Result (calculated earlier): ${items[namespacedId].renewable}`);
        if (verbose) console.groupEnd();
        return items[namespacedId].renewable;
    }

    // Check if item has any methods
    if (items[namespacedId].survival === undefined) {
        if (items[namespacedId].methods === undefined || items[namespacedId].methods.length == 0) {
            items[namespacedId].survival = false;
        } else {
            items[namespacedId].survival = true;
        }
    }

    if (__lastCount[namespacedId] === undefined) {
        __lastCount[namespacedId] = count;
    } else {
        let lastCount = __lastCount[namespacedId];
        // Item is currently being calculated, which means that the item has to calculate the renewability, not the method
        if (verbose) console.log(`Result (self-referenced): ${lastCount > count}`);
        if (verbose) console.groupEnd();
        return lastCount > count;
    }

    // Check if item is renewable
    for (methodData of items[namespacedId].methods) {
        if (getMethodRenewability(methodData.id, count / methodData.count)) {
            items[namespacedId].renewable = true;
            break;
        }
    }
    if (items[namespacedId].renewable !== true) items[namespacedId].renewable = false;

    __lastCount[namespacedId] = undefined;
    if (verbose) console.log(`Result: ${items[namespacedId].renewable}`);
    if (verbose) console.groupEnd();
    return items[namespacedId].renewable;
}



// Calculates tag renewability
function getTagRenewability(tagId, count) {
    if (verbose) console.groupCollapsed(`Tag \t| ${tagId} (x${count}):`);

    // If it's already calculated, use the calculated value
    if (tags[tagId].renewable !== undefined) {
        if (verbose) console.log(`Result (calculated earlier): ${tags[tagId].renewable}`);
        if (verbose) console.groupEnd();
        return tags[tagId].renewable;
    }


    let tagRenewability = false;
    let isSurvival = false;
    tags[tagId].renewable = null;

    for (id of tags[tagId].items) {
        if (getItemRenewability(id, count)) {
            tagRenewability = true;
        }
        if (id[0] == "#") {
            if (tags[id] !== undefined && tags[id].survival)
                isSurvival = true;
        } else {
            if (items[id] !== undefined && items[id].survival)
                isSurvival = true;
        }
        
        if (tagRenewability) {
            break;
        }
    }
    tags[tagId].survival = isSurvival;
    tags[tagId].renewable = tagRenewability;

    if (verbose) console.log(`Result: ${tags[tagId].renewable}`);
    if (verbose) console.groupEnd();
    return tags[tagId].renewable;
}



// Calculates method renewability
function getMethodRenewability(methodId, multiplier) {
    if (verbose) console.groupCollapsed(`Method \t| ${methodId} (x${multiplier}):`);

    // If it's already calculated, use the calculated value
    if (methods[methodId].renewable !== undefined) {
        if (verbose) console.log(`Result (calculated earlier): ${methods[methodId].renewable}`);
        if (verbose) console.groupEnd();
        return methods[methodId].renewable;
    }

    let dependencies = methods[methodId].dependencies;
    if (typeof dependencies === "boolean" || dependencies === null) {
        if (dependencies) {
            // dependencies === true; no dependencies but not renewable
            if (verbose) console.log(`Result (dependencies == true): ${false}`);
            if (verbose) console.groupEnd();
            methods[methodId].renewable = false;
            return false;
        } else {
            // dependencies === false || dependencies === null; no dependencies, same as []
            dependencies = [];
        }
    }

    for (let dependency of itemData(dependencies, true)) {
        let itemRenewability;
        if (Array.isArray(dependency)) {
            itemRenewability = false
            for (element of dependency) {
                if (getItemRenewability(element.item, element.count * multiplier)) {
                    itemRenewability = true;
                    break;
                }
            }
        } else {
            itemRenewability = getItemRenewability(dependency.item, dependency.count * multiplier)
        }
        if (!itemRenewability) {
            if (verbose) console.log(`Result: ${false}`);
            if (verbose) console.groupEnd();
            methods[methodId].renewable = false;
            return methods[methodId].renewable;
        }
    }

    if (verbose) console.log(`Result: ${true}`);
    if (verbose) console.groupEnd();
    methods[methodId].renewable = true;
    return methods[methodId].renewable;
}



// Opens the page from url
function updatePages() {
    let arr = location.hash.split("#");
    arr.shift();
    changePage(...arr);
}



// Currently opened page
let currentPage = {}
let isMainPageGenerated = false;

function changePage(page = "main", ...args) {
    document.querySelector("#loadingText").style.display = "block"
    setTimeout(loadPage, 1, page, ...args)
}

function loadPage(page = "main", ...args) {
    currentPage.page = page;
    currentPage.args = [args];

    document.querySelectorAll(".page").forEach((element) => {element.classList.remove("active")});
    if (page === "main") {
        // MAIN PAGE
        document.querySelector("#mainPage").classList.add("active");

        if (!isMainPageGenerated) {
            // Generate the main page
            let itemList = document.querySelector("#mainPage #itemList");
            itemList.innerHTML = "";
                
            for (let i = 0; i < Object.keys(items).length; i++) {
                let listElementNode = createItemListElement(Object.keys(items)[i], i + 1, undefined, true);
                itemList.appendChild(listElementNode);
            }

            isMainPageGenerated = true;
        }
    
    } else if (page === "item") {
        // ITEM PAGE
        document.querySelector("#itemPage").classList.add("active");

        if (args[0] === undefined || (items !== undefined && items[args[0]] === undefined)) {
            console.error("Item not found");
            return;
        }
        
        let itemId = args[0];
        let item;
        if (items !== undefined) {
            item = items[itemId];
        } else {
            item = {name: "Item", renewable: false, methods: []};
        }

        // Update metadata
        document.querySelector("#itemPage #icon").style.backgroundImage = `url('./assets/img/item/${itemId.replace(":", "/")}.png')`;
        document.querySelector("#itemPage #name").innerText = item.name;
        document.querySelector("#itemPage #id").innerText = itemId;
        if (item.survival) {
            document.querySelector("#itemPage #renewable").style.display = item.renewable ? "block" : "none";
            document.querySelector("#itemPage #notRenewable").style.display = item.renewable ? "none" : "block";
            document.querySelector("#itemPage #notSurvival").style.display = "none";
        } else {
            document.querySelector("#itemPage #notSurvival").style.display = "block";
            document.querySelector("#itemPage #renewable").style.display = "none";
            document.querySelector("#itemPage #notRenewable").style.display = "none";
        }

        let methodList = document.querySelector("#itemPage #methodList");
        methodList.innerHTML = "";

        if (item.methods.length == 0) {
            methodList.innerHTML = "None".italics();
        } else {
            for (let i = 0; i < item.methods.length; i++) {
                let methodId = item.methods[i].id;
                let listElement = createMethodListElement(methodId, i + 1);
                methodList.appendChild(listElement);
            }
        }

    } else if (page === "method") {
        // METHOD PAGE
        document.querySelector("#methodPage").classList.add("active");

        // Check if method with given id exists
        if (args[0] === undefined || isNaN(parseInt(args[0])) || (methods !== undefined && methods[parseInt(args[0])] === undefined)) {
            console.error("Method not found");
            return;
        }
        
        let methodId = args[0];
        let method;
        if (items !== undefined) {
            method = methods[methodId];
        } else {
            method = {name: "Method", icon: "icon/none", description: "Description", renewable: false, results: [], dependencies: true};
        }

        // Update metadata
        document.querySelector("#methodPage #icon").style.backgroundImage = `url('./assets/img/${method.icon}.png')`;
        document.querySelector("#methodPage #name").innerText = method.name;
        document.querySelector("#methodPage #description").innerText = method.description;
        document.querySelector("#methodPage #renewable").style.display = method.renewable ? "block" : "none";
        document.querySelector("#methodPage #notRenewable").style.display = method.renewable ? "none" : "block";


        let resultList = document.querySelector("#methodPage #resultList");
        resultList.innerHTML = "";
        if (method.results.length == 0) {
            resultList.innerHTML = "None".italics();
        } else {
            for (let i = 0; i < method.results.length; i++) {
                let itemId = method.results[i].item;
                resultList.appendChild(createItemListElement(itemId, i + 1, method.results[i].count));
            }
        }

        let dependencyList = document.querySelector("#methodPage #dependencyList");
        dependencyList.innerHTML = "";
        if (!Array.isArray(method.dependencies) || method.dependencies.length == 0) {
            dependencyList.innerHTML = "None".italics();
        } else {
            for (let i = 0; i < method.dependencies.length; i++) {
                let dependency = method.dependencies[i];
                if (Array.isArray(dependency)) {
                    let container = document.createElement("div");
                    container.style.paddingLeft = "20px";

                        let containerHeader = document.createElement("h3");
                        containerHeader.innerText = "One of the following:"
                        container.appendChild(containerHeader)

                        for (let j = 0; j < dependency.length; j++) {
                            container.appendChild(createItemListElement(dependency[j].item, (i + 1) + "." + (j + 1), dependency[j].count))
                        }

                    dependencyList.appendChild(container);
                } else {
                    dependencyList.appendChild(createItemListElement(dependency.item, i + 1, dependency.count));
                }
            }
        }
    }

    document.querySelector("#loadingText").style.display = "none";
}



function loadData() {
    jsonDirectory = "assets/data/generated/";
    downloadFile(jsonDirectory + "items.json",   (data) => {items   = JSON.parse(data); finalizeLoading();});
    downloadFile(jsonDirectory + "methods.json", (data) => {methods = JSON.parse(data); finalizeLoading();});
    downloadFile(jsonDirectory + "tags.json",    (data) => {tags    = JSON.parse(data); finalizeLoading();});
}

let lock = false;
function finalizeLoading() {
    if (items === undefined ||
        methods === undefined ||
        tags === undefined) {
        return;
    }
    if (lock) return;
    lock = true;


    let timer;
    timer = Date.now();
    function showTime(message) {
        console.log(`${Date.now()-timer} ms \t|`, message);
        timer = Date.now();
    }

    console.group("Init (post-download)");


    // Clean up downloaded data (convert item strings to objects and non-array data to single element arrays)
    for (let methodId in methods) {
        let method = methods[methodId];
        if (method.results === undefined || method.results === null || method.results === "" || method.results === []) {
            console.warn(`Method #${methodId} has no results (results == undefined, null, "" or []), which makes it useless`, method, `Setting results to []`);
            method.results = [];
        }
        method.results = itemData(method.results, true);
        if (typeof method.dependencies !== "boolean" && method.dependencies !== null) {
            method.dependencies = itemData(method.dependencies, true);
        }
    }

    // Show time
    showTime("Cleaning up method results and dependencies");


    // Assign methods to items
    for (let itemId in items) {

        // Array of objects with {id: methodId, count: resultingItemCount}
        let itemMethods = [];
        for (let methodId = 0; methodId < methods.length; methodId++) {
            
            // One (string) or more (array) items in the form of a string (with item id) or an object (with {item: namespacedItemId, count: itemCount})
            for (let result of itemData(methods[methodId].results, true)) {
                // Check if the resulting item is the item that we want
                if (result.item === itemId) {
                    // If yes, add to the array
                    itemMethods.push({
                        id: methodId,
                        count: result.count
                    });
                }
            }
        }
        // Copy array to the item
        items[itemId].methods = itemMethods;
    }

    // Show time
    showTime("Assigning methods to items");


    console.groupCollapsed("Calculating item renewability");

    // Calculate item renewability
    numberOfRenewableItems = 0;
    numberOfNonSurvivalItems = 0;
    for (let item in items) {
        numberOfRenewableItems += getItemRenewability(item);
        numberOfNonSurvivalItems += items[item].survival ? 0 : 1;
    }

    console.groupEnd();

    // Show time
    showTime("Calculating item renewability");


    console.groupCollapsed("Calculating tag renewability");

    // Calculate tag renewability
    for (let tag in tags) {
        getTagRenewability(tag);
    }

    console.groupEnd();

    // Show time
    showTime("Calculating tag renewability");


    console.groupCollapsed("Calculating method renewability");

    // Calculate method renewability
    for (let i = 0; i < methods.length; i++) {
        getMethodRenewability(i);
    }

    console.groupEnd();

    // Show time
    showTime("Calculating method renewability");
    


    // Update header numbers
    document.querySelector("#header #renewableItemCount").innerText = `Renewable items: ${numberOfRenewableItems}/${Object.keys(items).length - numberOfNonSurvivalItems} (${Math.floor((numberOfRenewableItems / (Object.keys(items).length - numberOfNonSurvivalItems)) * 10000) / 100}%)`;

    document.querySelector("#header #notSurvivalItemCount").innerText = `Non-survival items: ${numberOfNonSurvivalItems} (${Math.floor((numberOfNonSurvivalItems / (Object.keys(items).length)) * 10000) / 100}%)`;


    console.groupEnd();

    // Update
    updatePages();
}

function init() {
    // Load JSON data
    loadData();

    window.addEventListener("hashchange", (e) => {
        updatePages();
    });
}

init();