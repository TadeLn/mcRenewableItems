import os
import json



def mergeAll(resultPath = "../generated/", prettyPrint = False, verbose = False):
    items = {}

    # Get all files from directory "items"
    path =  "items"
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files.sort()

    # Load data from every file
    for file in files:
        filePath = os.path.join(path, file)
        if file[0] == "_":
            print("Skipping file " + filePath + " (has _ at the beginning)")
            continue
        print("Loading item data from " + filePath)

        with open(filePath) as f:
            fileData = json.load(f)

            for itemId in fileData:
                if verbose:
                    print("Adding item: " + itemId)
                item = fileData[itemId]
                items[itemId] = item
        print("----------")
              
    resultFilePath = resultPath + "items.json"
    with open(resultFilePath, "w") as f:
        print("\n============================================================")
        if prettyPrint:
            json.dump(items, f, indent = 4)
            print("Done! Result saved to " + resultFilePath + " (with pretty print)")
        else:
            json.dump(items, f)
            print("Done! Result saved to " + resultFilePath)
        print("============================================================")
        # print(items)



    methods = []

    # Get all files from directory "methods"
    path = "methods"
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files.sort()

    # Load data from every file
    for file in files:
        filePath = os.path.join(path, file)
        if file[0] == "_":
            print("Skipping file " + filePath + " (has _ at the beginning)")
            continue
        print("Loading method data from " + filePath)

        with open(filePath) as f:
            fileData = json.load(f)

            for methodId in range(len(fileData)):
                if verbose:
                    print("Adding method: ", methodId)
                method = fileData[methodId]
                methods.append(method)
        print("----------")
              
    resultFilePath = resultPath + "methods.json"
    with open(resultFilePath, "w") as f:
        print("\n============================================================")
        if prettyPrint:
            json.dump(methods, f, indent = 4)
            print("Done! Result saved to " + resultFilePath + " (with pretty print)")
        else:
            json.dump(methods, f)
            print("Done! Result saved to " + resultFilePath)
        print("============================================================")
        # print(methods)



    tags = {}

    # Get all files from directory "tags"
    path = "tags"
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    files.sort()

    # Load data from every file
    for file in files:
        filePath = os.path.join(path, file)
        if file[0] == "_":
            print("Skipping file " + filePath + " (has _ at the beginning)")
            continue
        print("Loading tag data from " + filePath)

        with open(filePath) as f:
            filedata = json.load(f)

            for tagId in filedata:
                if verbose:
                    print("Adding tag: " + tagId)
                tag = filedata[tagId]
                tags[tagId] = tag
        print("----------")
              
    resultFilePath = resultPath + "tags.json"
    with open(resultFilePath, "w") as f:
        print("\n============================================================")
        if prettyPrint:
            json.dump(tags, f, indent = 4)
            print("Done! Result saved to " + resultFilePath + " (with pretty print)")
        else:
            json.dump(tags, f)
            print("Done! Result saved to " + resultFilePath)
        print("============================================================")
        # print(tags)

if __name__ == '__main__':
    mergeAll(prettyPrint = True, verbose = True)