import merge
import recipes
import tags

def generate():
    recipes.generateRecipes(verbose = True)
    tags.generateTags(verbose = False)
    merge.mergeAll()

if __name__ == '__main__':
    generate()