import os
import json
import globals



def convertItem(item, setCount = False):
    if isinstance(item, list):
        result = []
        for alternative in item:
            result.append(convertItem(alternative, setCount))
        return result
    else:
        count = item.get("count", 1)
        if setCount != False:
            count = setCount

        if "item" in item:
            return {"item": item["item"], "count": count}
        elif "tag" in item:
            return {"item": "#" + item["tag"], "count": count}
        else:
            print("convertItem: Incorrect data: Couldn't find 'item' or 'tag' property in item data. Ignoring item")



def generateRecipes(verbose = False):
    methods = []

    # Get all files from directory with recipes
    path = os.path.join(globals.mcDataDir, "minecraft", "recipes")
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    print("Generating recipe methods")
    # Load data from every file
    for file in files:
        filePath = os.path.join(path, file)
        if verbose:
            print("Loading method data from " + filePath)

        with open(filePath) as f:
            fileData = json.load(f)
            if verbose:
                print(fileData)

            # Special crafting recipes often add multiple recipes manually
            # and should unset the add method variable to not add the default method
            addMethod = True
            namePrefix = "Recipe"
            icon = "icon/recipe"
            method = {
                "type": "recipe",
                "description": "A recipe extracted from the recipe book",
                "recipeData": fileData
            }
            
            smeltingTypes = {
                "minecraft:smelting": ["Smelting", "icon/smelting"],
                "minecraft:smoking": ["Smoking", "icon/smoking"],
                "minecraft:blasting": ["Blasting", "icon/blasting"],
                "minecraft:campfire_cooking": ["Campfire Cooking", "icon/campfire_cooking"]
            }

            # Shaped crafting
            if fileData["type"] == "minecraft:crafting_shaped":
                namePrefix = "Shaped Crafting"
                icon = "icon/crafting"
                keyArr = []
                for line in fileData["pattern"]:
                    for char in list(line):
                        keyArr.append(char)
                keys = {}
                for key in keyArr:
                    if (key == ' '):
                        continue
                    keys[key] = keys.get(key, 0) + 1
                method["dependencies"] = []
                for key in keys:
                    method["dependencies"].append(convertItem(fileData["key"][key], keys[key]))
                    
                method["results"] = [{
                    "item": fileData["result"]["item"],
                    "count": fileData["result"].get("count", 1)
                }]
            
            # Shapeless crafting
            elif fileData["type"] == "minecraft:crafting_shapeless":
                namePrefix = "Shapeless Crafting"
                icon = "icon/crafting"
                itemIds = {}
                for itemData in fileData["ingredients"]:
                    if "item" in itemData:
                        itemIds[itemData["item"]] = itemIds.get(itemData["item"], 0) + 1
                    else:
                        if isinstance(itemData, list):
                            # TODO
                            pass
                        else:
                            itemIds["#" + itemData["tag"]] = itemIds.get("#" + itemData["tag"], 0) + 1

                method["dependencies"] = []
                for itemId in itemIds:
                    method["dependencies"].append({
                        "item": itemId,
                        "count": itemIds[itemId]
                    })
                method["results"] = [{
                    "item": fileData["result"]["item"],
                    "count": fileData["result"].get("count", 1)
                }]
            
            # Stonecutting
            elif fileData["type"] == "minecraft:stonecutting":
                namePrefix = "Stonecutting"
                icon = "icon/stonecutting"
                method["dependencies"] = [convertItem(fileData["ingredient"])]
                method["results"] = [{
                    "item": fileData["result"],
                    "count": fileData.get("count", 1)
                }]
            
            # Smithing
            elif fileData["type"] == "minecraft:smithing":
                namePrefix = "Smithing"
                icon = "icon/smithing"
                method["dependencies"] = [
                    convertItem(fileData["base"]),
                    convertItem(fileData["addition"])
                ]
                method["results"] = [{
                    "item": fileData["result"]["item"],
                    "count": fileData.get("count", 1)
                }]
            
            # Smelting recipes:
            elif fileData["type"] in smeltingTypes:
                namePrefix = smeltingTypes[fileData["type"]][0]
                icon = smeltingTypes[fileData["type"]][1]

                method["dependencies"] = [convertItem(fileData["ingredient"])]

                method["results"] = [{
                    "item": fileData["result"],
                    "count": fileData.get("count", 1)
                }]
            
            # Special craftings:
            # minecraft:crafting_special_shulkerboxcoloring
            elif fileData["type"] == "minecraft:crafting_special_shulkerboxcoloring":
                namePrefix = "Special Crafting"
                # TODO
                pass
            else:
                print("UNHANDLED_RECIPE_TYPE", fileData["type"])

            method["name"] = namePrefix + ": " + file.split(".")[0].replace("_", " ").title()
            method["icon"] = icon
                        
            if addMethod:
                methods.append(method)
        if verbose:
            print("----------")
              
    resultFilePath = "methods/recipes.json"
    with open(resultFilePath, "w") as f:
        print("\n============================================================")
        json.dump(methods, f)
        print("Done! Result saved to " + resultFilePath)
        print("============================================================")
        # print(methods)

if __name__ == '__main__':
    generateRecipes(verbose = True)