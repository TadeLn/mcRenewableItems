import os
import json
import globals



def generateTags(verbose = False):
    tags = {}

    files = []

    # Get all files from directory with tags
    path = os.path.join(globals.mcDataDir, "minecraft", "tags", "items")
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    print("Generating tags")
    # Load data from every file
    for file in files:
        filePath = os.path.join(path, file)
        if verbose:
            print("Loading tag data from " + filePath)

        with open(filePath) as f:
            fileData = json.load(f)
            if verbose:
                print(fileData)

            tagName = "#minecraft:" + file.split(".")[0]
            if tagName not in tags:
                tags[tagName] = {"name": tagName, "items": []}
            if fileData["replace"]:
                tags[tagName]["items"] = []
            for itemId in fileData["values"]:
                if itemId not in tags[tagName]["items"]:
                    tags[tagName]["items"].append(itemId)

              
    resultFilePath = "tags/minecraft.json"
    with open(resultFilePath, "w") as f:
        print("\n============================================================")
        json.dump(tags, f)
        print("Done! Result saved to " + resultFilePath)
        print("============================================================")
        # print(tags)

if __name__ == '__main__':
    generateTags(verbose = True)